package edu.sjsu.android.exercise8tonyxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private MyReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.status);
        receiver = new MyReceiver(textView);
        registerReceiver(receiver, new IntentFilter(DownloadService.NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    public void startDownload(View view) {
        // TODO: sent an explicit Intent to start DownloadService
        // Hint: similar as starting an activity class
        Intent explicit = new Intent(this, DownloadService.class);

        // TODO: add following data to the above Intent:
        // filename: "index.html"
        // urlpath: "https://www.sjsu.edu/cs/index.html"
        // by the corresponding keys (static Strings in DownloadService)
        explicit.putExtra(DownloadService.FILENAME, "index.html");
        explicit.putExtra(DownloadService.URLPATH, "https://www.sjsu.edu/cs/index.html");

        // TODO: start the service using the above Intent
        textView.setText("Download started");
        DownloadService.enqueueWork(this, explicit);
    }
}