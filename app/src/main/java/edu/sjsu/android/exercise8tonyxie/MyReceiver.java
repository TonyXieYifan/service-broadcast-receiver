package edu.sjsu.android.exercise8tonyxie;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;

public class MyReceiver extends BroadcastReceiver {

    TextView textview;

    public MyReceiver(TextView textView){
        super();
        this.textview = textView;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: get the filepath (String) and resultCode (int) from the intent
        // by the corresponding keys (static Strings in DownloadService)
        // Hint: when getting int, you need to set a default value
        // Set it to Activity.RESULT_CANCELED
        String filepath = intent.getStringExtra(DownloadService.FILEPATH);
        int resultCode = intent.getIntExtra(DownloadService.RESULT, Activity.RESULT_CANCELED);

        // TODO: If resultCode is Activity.RESULT_OK,
        // set textview as "Download done. Filepath: " + filepath
        // else, set textview as "Download failed".
        if(resultCode == Activity.RESULT_OK){
            textview.setText("Download done. Filepath: " + filepath);
        }else{
            textview.setText("Download failed");
        }
    }
}
