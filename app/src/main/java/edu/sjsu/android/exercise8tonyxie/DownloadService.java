package edu.sjsu.android.exercise8tonyxie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class DownloadService extends JobIntentService {

    // Keys for extras used for the service
    public static final String URLPATH = "edu.sjsu.android.exercise8.urlpath";
    public static final String FILENAME = "edu.sjsu.android.exercise8.filename";
    // Keys for extras used for the broadcast receiver
    public static final String FILEPATH = "edu.sjsu.android.exercise8.filepath";
    public static final String RESULT = "edu.sjsu.android.exercise8.result";

    public static final String NOTIFICATION = " edu.sjsu.android.exercise8";

    private int result = Activity.RESULT_CANCELED;

    static void enqueueWork(Context context, Intent work){
        enqueueWork(context, DownloadService.class, 1, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        // TODO: return if external storage is not available
        // Hint: use the private helper method you implemented in 2.2
        if(!isExternalStorageWritable()){
            return;
        }
        // TODO: get the urlPath and filename from the intent
        // by the corresponding keys
        String urlpath = intent.getStringExtra(URLPATH);
        String filename = intent.getStringExtra(FILENAME);;
        if (filename == null) return;
        // TODO: set path to the path to private external storage (the root)
        File path = getBaseContext().getExternalFilesDir(null);
        File output = new File(path, filename);
        // TODO: call saveFile, pass in urlpath and output
        // Set result to Activity.RESULT_OK only if saveFile returns true
        if(saveFile(urlpath, output)){
            result = Activity.RESULT_OK;
        }
        // TODO: call publishResults, pass in output.getAbsolutePath()
        publishResults(output.getAbsolutePath());
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void publishResults(String filepath) {
        Intent intent = new Intent(NOTIFICATION);
        // TODO: add the filepath and the result to the above intent
        // using the corresponding keys
        intent.putExtra(FILEPATH, filepath);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }

    private boolean saveFile(String urlpath, File output){
        try {
            // Fetch the file from the urlpath
            URL url = new URL(urlpath);
            InputStream stream = url.openConnection().getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            // Save (Write) the file to output
            FileOutputStream fos = new FileOutputStream(output.getPath());
            int next;
            while ((next = reader.read()) != -1) {
                fos.write(next);
            }
            // Close streams and return true after done
            stream.close();
            fos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
